var bomSekarang = [3,5,7]
var barisYgDipilih = 0
var pilihanke = 1
var bomTerpilih = []
var giliranSiapa = 'giliran kamu: '
var emot = ['💣', '🥪', '🍩', '🍕', '🍮', '☕️', '🥝', '🍪']

function acak(min, max) {
	return Math.floor(Math.random() * (max-min+1)) + min
}

const pasangBom = () => {
	var jumlah = bomSekarang.reduce((a, i) => a+i, 0)
	if (jumlah == 1) {
		return bomb
	}

	var ygtidakkosong = []
	bomSekarang.forEach((i, idx) => {
		if (i != 0) {ygtidakkosong.push(idx)}
	})
	var jumlahbaris = ygtidakkosong.length

	var hasil = ``
	bomSekarang.forEach((i, idx) => {
		hasil+=`<div class="baris" id="baris${idx+1}">`
		for (var j = 0; j < i; j++) {
			hasil+= `<span class="bomb" id="bomb${idx}-${j}"`
			if (giliranSiapa === 'giliran kamu: ') {
				if (jumlahbaris > 1) {
					hasil+=` onclick="bomb(${idx}, ${j})"`
				} else {
					if (j != 0) {
						hasil+=` onclick="bomb(${idx}, ${j})"`
					}
				}
			}
			hasil+=`>${emotsekarang}</span>`
		}
		hasil+=`</div>`
	})
	return hasil
}

function kalah() {
	if (giliranSiapa === 'giliran komputer: ') {
		$('#giliran').html('kamu kalah')
		$('.pilihan').html(`<h1>💥</h1>`)
	} else {
		$('#giliran').html('kamu menang')
		$('.pilihan').html(`<h1>🎉</h1>`)
	}
	$('#keterangan').html('')
	$('#ambil').show()
	$('#ambil').html('main lagi')
}

function bomb(baris = 0, ke = 0) {
	
	var jumlah = bomSekarang.reduce((a, i) => a+i, 0)
	if (jumlah == 1) {
		kalah()
		return
	}

	if(pilihanke === 1) {
		barisYgDipilih = baris
	} else {
		if (barisYgDipilih !== baris) {
			barisYgDipilih = baris
			pilihanke = 1
			bomTerpilih = []
			$('.bomb').removeClass('dipilih')
		}
	}
	var ada = false
	bomTerpilih.forEach((i) => {
		if (i === ke) ada = true
	})
	if (!ada) {
		bomTerpilih.push(ke)
		document.getElementById('bomb'+baris+'-'+ke).classList.add('dipilih')
		pilihanke += 1
	} else {
		$('#bomb'+baris+'-'+ke).removeClass('dipilih')
		pilihanke-=1
		bomTerpilih = bomTerpilih.filter(i => {
			if (!(i === ke)) {
				return i
			}
		})
	}
	$('#keterangan').html(`memilih ${bomTerpilih.length} di grup ${barisYgDipilih+1}`)
}

function ambil() {
	if($('#ambil').text() == 'main lagi') {
		mulaiAwal()
		return
	}
	if (bomTerpilih.length==0) {return}
	
	bomSekarang[barisYgDipilih] = bomSekarang[barisYgDipilih]-bomTerpilih.length
	barisYgDipilih = 0
	pilihanke = 1
	bomTerpilih = []
	var jumlah = bomSekarang.reduce((a, i) => a+i, 0)
	if (jumlah > 1) {
		if (giliranSiapa === 'giliran kamu: ') {
			giliranKomputer()
		} else {
			giliranSiapa = 'giliran kamu: '
			$('#giliran').html(giliranSiapa)
			$('#ambil').show()
		}
	}
	$('.pilihan').html(pasangBom())
}

function giliranKomputer() {
	var ygtidakkosong = []
	bomSekarang.forEach((i, idx) => {
		if (i != 0) {ygtidakkosong.push(idx)}
	})
	var jumlahbaris = ygtidakkosong.length
	var barisKomputer = ygtidakkosong[acak(0, ygtidakkosong.length-1)]
	var bombKomputer = acak(1, bomSekarang[barisKomputer])

	if (jumlahbaris === 1) {
		bombKomputer = acak(1, bomSekarang[barisKomputer]-1)
	}

	$('#ambil').hide()
	giliranSiapa = 'giliran komputer: '
	$('#giliran').html(giliranSiapa)
	
	setTimeout(komputerJalan, 1000, barisKomputer, bombKomputer-1)
}

function komputerJalan(ba, bo) {
	bomb(ba, bo)
	$('#bomb'+ba+'-'+bo).hide()
	
	if (bo > 0) {
		setTimeout(komputerJalan, 1000, ba, bo-1)
	} else {
		setTimeout(function () {
			ambil()
		}, 1000)
	}
}

function mulaiAwal() {
	bomSekarang = [3,5,7]
	barisYgDipilih = 0
	pilihanke = 1
	bomTerpilih = []
	giliranSiapa = 'giliran kamu: '
	emotsekarang = emot[acak(0, emot.length-1)]

	$('.pilihan').html(pasangBom())

	$('#ambil').html('ambil')
	$('#giliran').html(giliranSiapa)
}

$(document).ready(() => {
	mulaiAwal()
})