var lvl = []

const isiLevel = () => {
	lvl = JSON.parse(JSON.stringify(semualevel))
}

isiLevel()

const manukPosisi = (at) => { // atas
	$('#'+manuk_id).css({
		top: at+'%',
	})
}

const jalan = (waktu) => {
	manuk_posisi+= 0.2
	tantangan_posisi-=0.1
	manukPosisi(manuk_posisi)
	cek()
	$('#kumpulan-tantangan').css({
		right: tantangan_posisi+'%'
	})
	if(jalankah) requestAnimationFrame(jalan)
}

const tampil_tameng = () => {
	var tameng_sekarang = `Level: ${level+1} <br>`
	for (var i = 0; i < tameng; i++) {
		tameng_sekarang += '🛡️'
	}
	$('#tameng').html(tameng_sekarang)
}

const cek = () => {
	if (lvl[level].tantangan.length <= 0) return cekmenangkah()

	var ygmana = lvl[level].tantangan[0]
	var kisaran_kalah = [ygmana[2] + tantangan_posisi - 5, ygmana[2] + tantangan_posisi+5]

	if (5 >= kisaran_kalah[0] && 5 <= kisaran_kalah[1]) {
		if (manuk_posisi < ygmana[0] || (manuk_posisi+5) > (100-ygmana[1])) {
			if (tameng > 0) {
				lvl[level].tantangan.shift()
				tameng -= 1

				tampil_tameng()
			} else {
				kalah()
			}
		}
	} else if(5 > kisaran_kalah[1]) {
		lvl[level].tantangan.shift()
	}
}

const cekmenangkah = () => {
	if ((lvl[level].titik_menang+tantangan_posisi) < 5) {
		return menang()
	}
}