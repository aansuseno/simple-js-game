const manuk_id = 'manuk-emoji' // id untuk burung
const background_emoji = ['💀','💡', '🔥', '🕯️']
const manuk_emoji = ['🐤', '🦇', '🦋', '🐉', '🐦', '🦅', '🗿']


const acak = (min, max) => {
	return Math.floor(Math.random() * (max-min+1)) + min
}

// membuat background tengkorak, lampu, dll
const buatTengkorak= (titik_menang) => {
	var tengkorak = ``
	for (var i = 0; i < acak(10, 50); i++) {
		tengkorak += `<div class="tengkorak" style="top: ${acak(0, 95)}%; right: ${acak(0, lvl[level].titik_menang)+100}%">${background_emoji[acak(0, background_emoji.length-1)]}</div>`
	}
	return tengkorak
}

// membuat garis atas dan bawah
const buatGarisAtasBawah= () => {
	return `<div class="garis-atas"></div><div class="garis-bawah"></div>`
}

// membuat garis-garis rintangan
const buatTantangan= (semuatantangan) => {
	var tantangan = ``

	semuatantangan.forEach((i) => {
		tantangan += `
		<div class="tantangan" style="bottom: ${100-i[0]}%; right: ${i[2]}%; height: ${i[0]}%"></div>
		<div class="tantangan" style="top: ${100-i[1]}%; right: ${i[2]}%; height: ${i[1]}%"></div>
		`
	})

	return tantangan
}

// membuat garis menang
const buatGarisMenang= (titik_menang) => {
	return `<div class="garis-menang" style="right: ${titik_menang}%"></div>`
}

// membuat pembungkus tantangan
const bukaTantangan= () => {
	return `<div id="kumpulan-tantangan" style="right: 0%; height: 100%; width: 100%;">`
}
const tutupTantangan= () => {
	return `</div>`
}

// buat burung
const buatBurung= () => {
	return `
		<span style="font-size: 5vmin; right: 5%" id="${manuk_id}">
			${manuk_emoji[acak(0, manuk_emoji.length-1)]}
		</span>
	`
}

const gambarSemua = (tantangan, titik_menang) => {
	var gambar_tengkorak = buatTengkorak(titik_menang)
	var gambar_garis_atas_bawah = buatGarisAtasBawah()
	var gambar_tantangan = buatTantangan(tantangan)
	var gambar_garis_menang = buatGarisMenang(titik_menang)
	var gambar_burung = buatBurung()

	return gambar_garis_atas_bawah + bukaTantangan() + gambar_tengkorak + gambar_tantangan + gambar_garis_menang + tutupTantangan() + gambar_burung
}