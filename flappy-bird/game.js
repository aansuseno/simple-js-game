const O_ = '#kotak-game' // id untuk game
var manuk_posisi = 0 // posisi burung sekarang
var level = 0
var tameng = 3
var tantangan_posisi = 0
var jalankah = true


const lompat = () => {
	manuk_posisi -= 4
}

const mulai = () => {
	jalankah = true
	manuk_posisi = 45
	tantangan_posisi=0
	$('#kotak-game').show()
	$('#sambutan').hide()
	var level_sekarang = lvl[level]
	$(O_).html(gambarSemua(level_sekarang.tantangan, level_sekarang.titik_menang))
	manukPosisi(0)
	jalan()
	tampil_tameng()
}

const menang =() => {
	jalankah = false
	level+=1

	if (level == lvl.length) {return gameHabis()}

	$('#kotak-game').hide()
	$('#sambutan').show()

	$('#pesan').html('Hore lanjut ke:')
	$('#tombol').html('Level '+(level+1))
}

const kalah = () => {
	jalankah = false

	$('#kotak-game').hide()
	$('#sambutan').show()

	$('#pesan').html('Oops kamu kalah :(')
	$('#tombol').html('Coba lagi')
	manuk_posisi = 0 // posisi burung sekarang
	level = 0
	tameng = 3
	tantangan_posisi = 0
	isiLevel()
	console.log(semualevel)
}

const gameHabis = () => {
	$('#kotak-game').hide()
	$('#sambutan').show()
	isiLevel()
	level = 0
	tameng = 3
	$('#pesan').html('Maaf level habis. Silakan jika mau menambah level atau memperbaiki game ini kunjungi <a href="https://gitlab.com/aansuseno/simple-js-game">gitlab</a> saya.')
	$('#tombol').html('Main Ulang')
}

$(document).ready(() => {
	// mulai()
	$('#banyaklevel').text(level.length)
	$('#tombol').click(() => {
		mulai()
	}) 

	// lompat
	$(document).keypress((evt) => {
		if((evt.which == 32 || evt.which == 119) && jalankah) {
			lompat()
		}
	})
	$(document).click(() => {
		if(jalankah) lompat()
	})
})