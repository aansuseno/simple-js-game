const emot = [
	['🖐️', 'kertas'],
	['✌️', 'gunting'],
	['✊', 'batu']
]
var poin = [0,0] //user,komputer

const pasangEmot = () => {
	var tampilemot = emot.map(i => {
		return `<span onclick="pilih('${i[1]}', '${i[0]}')">${i[0]}</span>`
	})
	return tampilemot
}

const pilih = (user, emotnya) => {
	var acak = Math.floor(Math.random() * (2-0+1)) + 0
	var komputer = emot[acak][1]

	$('#pilihan').hide()
	$('#hasil').show()
	$('#hasilKomputer').html(emot[acak][0])
	$('#hasilUser').html(emotnya)

	if(komputer === user) {
		return seri()
	}
	
	if(user === 'gunting') {
		return (komputer === 'kertas') ? menang() : kalah()
	} else if (user === 'kertas') {
		return (komputer === 'batu') ? menang() : kalah()
	} else if(user === 'batu') {
		return (komputer === 'gunting') ? menang() : kalah()
	}
}

const seri = () => {
	console.log('seri')
}

const menang = () => {
	poin[0] = poin[0]+1
	console.log('menang')
	updateSkor()
}

const kalah = () => {
	poin[1] = poin[1]+1
	console.log('kalah')
	updateSkor()
}

const updateSkor = () => {
	$('#skoruser').html(poin[0])
	$('#skorkomputer').html(poin[1])
}

$(document).ready(() => {
	$('.pilihan').html(pasangEmot())
	updateSkor()
	$('#mulailagi').click(() => {
		$('#pilihan').show()
		$('#hasil').hide()
	})
})